<?php
	$name = 'Alexey Khokhlov';
	$born = '21/09/1981';
	$email = 'a89200455525@ya.ru';
	$city = 'Russia, Nizhniy Novgorod';
	$about = 'I wanna write codes';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>About me</title>
	<style>
		.wrapper{
			width: 900px;
			margin: 5% auto;
			background: #cfcfcf;
			padding:10px;
		}
		td{
			text-align: center;
		}

	</style>
</head>
<body>
	<div class="wrapper">
		<h1>Personal page student of PHP-14: <?php echo ' '.$name; ?></h1>
			<table>
				<tr>
					<td>Date of birth:</td><td><?php echo $born; ?></td>
				</tr>
				<tr>
					<td>Email:</td><td><?php echo $email; ?></td>
				</tr>
				<tr>
					<td>City:</td><td><?php echo $city; ?></td>
				</tr>
				<tr>
					<td>About:</td><td><?php echo $about; ?></td>
				</tr>
			</table>
	</div>
</body>
</html>